from django.apps import AppConfig


class NumSimConfig(AppConfig):
    name = 'num_sim'
