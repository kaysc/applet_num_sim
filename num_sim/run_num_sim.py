from matplotlib import pyplot as plt
import io
import urllib, base64

# calculate change of GF over the next timestep
# with concentrations GF and E at the current one
def d_GF(GF_t, E_t, EGF_t, parameters):
    # first forth
    d_GF = -parameters["k_1"] * GF_t * E_t * parameters["d_t"]
    # first back
    d_GF += +parameters["k_1_back"] * EGF_t * parameters["d_t"]
    return d_GF


# calculate change of E over the next timestep
# with concentrations GF, EF, EGF, E, and F at the current one
def d_E(EF_t, GF_t, EGF_t, E_t, F_t, parameters):
    # first forth
    d_E = -parameters["k_1"] * E_t * GF_t * parameters["d_t"]
    # first back
    d_E += +parameters["k_1_back"] * EGF_t * parameters["d_t"]
    # third forth (assuming water excess)
    d_E += +parameters["k_3"] * EF_t * parameters["d_t"]
    # third forth
    d_E += -parameters["k_3_back"] * E_t * F_t * parameters["d_t"]
    return d_E


# calculate change of EGF over the next timestep
# with concentrations GF, E, and EGF at the current one
def d_EGF(GF_t, E_t, EGF_t, parameters):
    # first forth
    d_EGF = +parameters["k_1"] * E_t * GF_t * parameters["d_t"]
    # first back
    d_EGF += -parameters["k_1_back"] * EGF_t * parameters["d_t"]
    # second
    d_EGF += -parameters["k_2"] * EGF_t * parameters["d_t"]
    return d_EGF


# calculate change of EF over the next timestep
# with concentrations EGF, E, F, and EF at the current one
def d_EF(EGF_t, EF_t, E_t, F_t, parameters):
    # second
    d_EF = +parameters["k_2"] * EGF_t * parameters["d_t"]
    # third forth (assuming water excess)
    d_EF += -parameters["k_3"] * EF_t * parameters["d_t"]
    # third back
    d_EF += +parameters["k_3_back"] * E_t * F_t * parameters["d_t"]
    return d_EF


# calculate change of G over the next timestep
# with concentrations EGF at the current one
def d_G(EGF_t, parameters):
    # second
    d_G = parameters["k_2"] * EGF_t * parameters["d_t"]
    return d_G


# calculate change of F over the next timestep
# with concentrations EF, E, and F at the current one
def d_F(EF_t, E_t, F_t, parameters):
    # third forth (assuming water excess)
    d_F = +parameters["k_3"] * EF_t * parameters["d_t"]
    # third forth
    d_F += -parameters["k_3_back"] * E_t * F_t * parameters["d_t"]
    return d_F


def run_sim(parameters):
    # Get all parameters from entry fields
    #for key, item in parameters.items():
    #    print(key, "\t", item)

    # Initialize starting conditions
    GF_0 = parameters["GF_0"]
    GF_t = GF_0
    E_0 = parameters["E_0"]
    E_t = E_0
    EGF_t = 0
    EF_t = 0
    G_t = 0
    F_t = 0  # parameters["F_0"]
    d_t = parameters["d_t"]
    t_end = parameters["t_end"]

    # Initialize arrays with first value of time series
    GF = [GF_t]
    E = [E_t]
    EGF = [EGF_t]
    EF = [EF_t]
    G = [G_t]
    F = [F_t]
    t = [0]

    # calculate number of discrete teps
    no_steps = int(t_end / d_t) + 1

    # Do "reaction" step by step
    for step in range(no_steps):
        time = step * d_t

        # Calculate new concentration after timestep
        # Hard coded check for negative values to counteract error arising from simple numerical solver
        GF_new = GF_t + d_GF(GF_t=GF_t, E_t=E_t, EGF_t=EGF_t, parameters=parameters)
        GF_new = GF_new if GF_new > 0 else 0.0

        EGF_new = EGF_t + d_EGF(GF_t=GF_t, E_t=E_t, EGF_t=EGF_t, parameters=parameters)
        EGF_new = EGF_new if EGF_new > 0 else 0.0

        EF_new = EF_t + d_EF(EGF_t=EGF_t, EF_t=EF_t, E_t=E_t, F_t=F_t, parameters=parameters)
        EF_new = EF_new if EF_new > 0 else 0.0

        G_new = GF_0 - GF_t - EGF_t
        G_new = G_new if G_new > 0 else 0.0

        E_new = E_t + d_E(EF_t=EF_t, GF_t=GF_t, EGF_t=EGF_t, E_t=E_t, F_t=F_t, parameters=parameters)
        E_new = E_new if E_new > 0 else 0.0

        F_new = GF_0 - EF_t - EGF_t - GF_t
        F_new = F_new if F_new > 0 else 0.0

        # save new concentrations to current concentrations
        GF_t = GF_new
        EGF_t = EGF_new
        EF_t = EF_new
        G_t = G_new
        E_t = E_new
        F_t = F_new

        # add concentrations to time series
        GF.append(GF_t)
        EGF.append(EGF_t)
        EF.append(EF_t)
        G.append(G_t)
        E.append(E_t)
        F.append(F_t)
        t.append(time)


    # Plot results
    plt.clf()
    plt.cla()
    plt.close()
    fig = plt.figure()
    gs = fig.add_gridspec(2, 3)
    ax = fig.add_subplot(gs[:, 0:2])
    l1, = ax.plot(t, GF, label="GF")
    l2, = ax.plot(t, EGF, label="EGF")
    l3, = ax.plot(t, EF, label="EF")
    l4, = ax.plot(t, G, label="G")
    l5, = ax.plot(t, E, label="E")
    l6, = ax.plot(t, F, label="F")
    labels = ["GF", "EGF", "EF", "G", "E", "F"]
    ax.set_ylabel("Conc. [$\mu$M]")
    ax.set_xlabel("time [s]")


    if parameters["make_inlet"]:
        #if G_t > 0.7*GF_0:
        #    ains = ax.inset_axes([0.6, 0.3, 0.35, 0.4])
        #else:
        #    ains = ax.inset_axes([0.60, 0.55, 0.35, 0.4])
        # ains = inset_axes(a, width=0.3, height=0.3,)
        # Plot results
        ains = fig.add_subplot(gs[0, 2])
        ains.plot(t, GF)
        ains.plot(t, EGF)
        ains.plot(t, EF)
        ains.plot(t, G)
        ains.plot(t, E)
        ains.plot(t, F)
        left, right = ax.get_xlim()
        ains.set_xlim([left * 0.05, right * 0.05])
        left, right = ax.get_ylim()
        ains.set_ylim([left * 0.1, E_0 * 1.1])
        ains.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))

    legax = fig.add_subplot(gs[1, 2])
    legax.axis("off")
    legax.legend(handles=[l1, l2, l3, l4, l5, l6], labels=labels, loc="center", frameon=False, prop={'size': 12})

    if not False:
        ax.set_title(r"$\frac{\Delta F}{\Delta t}=$" + str(
            round(F[int(len(F) / 2)] / t[int(len(F) / 2)], 2)) + r" $\mu Ms^{-1}$ up to " + str(
            round(t[int(len(F) / 2)], 0)) + " s")
    ax.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
    plt.tight_layout()
    fig = plt.gcf()
    # confert graph into dtring buffer and then we convert 64 bit code into image
    buf = io.BytesIO()
    fig.savefig(buf, format="png")
    buf.seek(0)
    string = base64.b64encode(buf.read())
    url = urllib.parse.quote(string)
    return url