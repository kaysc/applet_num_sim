from django.shortcuts import render
import matplotlib.pyplot as plt
import io
import urllib, base64
from . import run_num_sim
from django.http import HttpResponse, StreamingHttpResponse

# Create your views here.

def home(request):
    # default parameters

    if not ("parameters" in locals() or "parameters" in globals()):
        parameters = {}
        parameters["k_1"] = 60.0 #100 # 600.0  # M^-1s^-1
        parameters["k_2"] = 10.0  # 20.0  # s^-1
        parameters["k_1_back"] = 1000.0  # 20.0  # s^-1
        parameters["k_3"] = 2.0  # 0.1  # s^-1
        parameters["k_3_back"] = 0.001  # 0.0 #1.0  # s^-1
        parameters["GF_0"] = 100.0  # M
        parameters["E_0"] = 10.0  # M
        parameters["d_t"] = 0.0001  # s
        parameters["t_end"] = 10.0  # s
        parameters["make_inlet"] = True
        default = parameters.copy()

        plt.plot()
        plt.axis("off")
        plt.xticks([])
        plt.yticks([])
        #plt.axes([0.4, 0.4, 0.2, 0.2])
        fig = plt.gcf()
        # confert graph into dtring buffer and then we convert 64 bit code into image
        buf = io.BytesIO()
        fig.savefig(buf, format="png")
        buf.seek(0)
        string = base64.b64encode(buf.read())
        init = urllib.parse.quote(string)
        parameters["plot"] = init
        fig.clear()

    if (request.GET.get('run_button')):
        para_list = [
                     "d_t", "t_end",
                     "k_1",
                     "E_0", "GF_0",
                     "k_1_back",
                     "k_2",
                     "k_3",
                     "k_3_back",
                     ]
        for para in para_list:
            parameters[para] = float(request.GET.get(para))
        # parameters["make_inlet"] = request.GET.get("make_inlet")
        url = run_num_sim.run_sim(parameters)
        parameters["plot"] = url

    if (request.GET.get('default_button')):
        parameters = default.copy()
        url = run_num_sim.run_sim(parameters)
        parameters["plot"] = url

    return render(request, 'home.html', parameters)
